from django.contrib import admin
from .models import Order, OrderRecord


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'owner', 'created_date')


class OrderRecordAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'product', 'quantity')



admin.site.register(OrderRecord, OrderRecordAdmin)
admin.site.register(Order, OrderAdmin)
