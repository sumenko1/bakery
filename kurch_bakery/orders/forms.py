from django import forms
from .models import OrderRecord
from django.contrib.auth import get_user_model


User = get_user_model()

class OrderRecordForm(forms.ModelForm):
    class Meta:
        model = OrderRecord
        fields = ('product', 'quantity')

