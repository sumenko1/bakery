from django.urls import path
from . import views

app_name = 'orders'

urlpatterns = [
    path('', views.orders_list, name='list'),
    path('order/<int:order_id>/', views.order_detail, name='detail'),
    path('order/create/', views.order_create, name='create'),
    path('order/<int:order_id>/remove/', views.order_remove, name='remove'),
    path('order/<int:order_id>/accept/', views.order_accept, name='accept'),
]
