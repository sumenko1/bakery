from datetime import datetime, timedelta
from django.urls import reverse
from django.utils.timezone import make_aware
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import (get_object_or_404, get_list_or_404, render,
                              redirect, HttpResponse)
from django.contrib.auth import get_user_model
from django.utils import timezone

from .models import OrderRecord, Order
from .forms import OrderRecordForm

# Наш список пользователей
User = get_user_model()

def calc_estimated_time():
    """ Вычисление оставшегося время и запись статуса в базу """
    orders = Order.objects.filter(accepted=True, finished=False)
    for order in orders:
        now = timezone.now()
        delta = now - order.started_date
        estimated_time = order.timeToCookTotal - delta
        if estimated_time.total_seconds() <= 0:
            estimated_time = timedelta(seconds=0)
            order.finished = True
        order.estimated_time = estimated_time
        order.save()
    

def orders_list(request):
    orders = Order.objects.select_related('owner')
    users = get_list_or_404(User)
    
    return render(request, 'orders/orders_list.html',
                  context={'orders': orders,
                           'users': users})


def order_detail(request, order_id):
    """ Детали заказа """
    form = OrderRecordForm(request.POST or None)
    order_content = None
    dt = None
    
    try:
        order_content = (OrderRecord.objects.select_related('product').
                         filter(order_id=order_id))
        dt = timedelta(seconds=calcCookTime_seconds(order_content))
    except ObjectDoesNotExist:
        pass

    if request.POST and form.is_valid():
        instance = form.save(commit=False)
        
        if order_content:
            for record in order_content:
                if (record.order.id == order_id and
                    record.product == instance.product):
                    instance.quantity += record.quantity

        OrderRecord.objects.update_or_create(order_id=order_id,
                                             product=instance.product,
                                             defaults={
                                                'quantity': instance.quantity})
        return redirect('orders:detail', order_id)
    order = get_object_or_404(Order, id=order_id)

    return render(request, 'orders/order_detail.html',
                  context={'order_content': order_content,
                           'timeToCookTotal': dt,
                           'form': form,
                           'order': order})


def order_create(request):
    """ Создание нового пустого заказа """
    uid = request.GET.get('uid')
    zero_time = timedelta(seconds=0)
    order = Order.objects.create(owner_id=uid,
                                 started_date=timezone.now(),
                                 timeToCookTotal=zero_time,
                                 estimated_time=zero_time)

    return redirect('orders:detail', order.id)


def add_product(request, order_id):
    """ Добавление продукта в заказ """
    form = OrderRecordForm(request.POST or None)

    if request.POST and form.is_valid():
        record = form.save(commit=False)

        record.order = get_object_or_404(Order, id=order_id)
        record.save()

    return render(request, 'orders/select_product.html', {'form': form})


def order_accept(request, order_id):
    """ Запуск заказа в исполнение """
    order = get_object_or_404(Order, id=order_id)
    try:
        order_content = OrderRecord.objects.filter(order=order)
        if not len(order_content):
            raise(ObjectDoesNotExist)
        timeToCookTotal = calcCookTime_seconds(order_content)
        order.timeToCookTotal = timedelta(seconds=timeToCookTotal)
        order.started_date = timezone.now()
        order.accepted = True
        order.save()

        # Чтобы сразу видеть остаток не дожидаясь планировщика
        calc_estimated_time()
        return redirect('orders:list')
    except ObjectDoesNotExist:
        return HttpResponse(f'Empty cart <a href="/order/{order_id}/">back<a>')



def order_remove(request, order_id):
    get_object_or_404(Order,id=order_id).delete()
    return redirect('orders:list')

def calcCookTime_seconds(order_content):
    """ Вычисляет время приготовления"""
    timeToCookTotal = 0

    for product in order_content:
        timeToCookTotal += (product.product.timeToCook.total_seconds() *
                            product.quantity)
    return timeToCookTotal
