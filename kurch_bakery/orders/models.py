from datetime import timedelta

from django.db import models
from django.contrib.auth import get_user_model
from products.models import Product

User = get_user_model()


class Order(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True,
                                        verbose_name='Время создания')
    started_date = models.DateTimeField(blank=True,
                                        verbose_name='Время начала')
    timeToCookTotal = models.DurationField(blank=True,
                                           verbose_name='Время приготовления')
    estimated_time = models.DurationField(default=timedelta(seconds=0),
                               verbose_name='Осталось времени')

    accepted = models.BooleanField(default=False,
                                   verbose_name='Принято в работу')
    finished = models.BooleanField(default=False,
                                   verbose_name='Заказ выполнен')

    products = models.ManyToManyField(
        Product,
        through='OrderRecord',
        through_fields=('order', 'product', 'quantity'))
    
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ('-created_date', )
    

class OrderRecord(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveSmallIntegerField(default=1,
                                                verbose_name='Количество')

    class Meta:
        verbose_name = 'Записи заказов'
        verbose_name_plural = 'Записи заказов'
        constraints = [
            models.UniqueConstraint(fields=['order', 'product'],
                                    name='order-elements')
        ]

