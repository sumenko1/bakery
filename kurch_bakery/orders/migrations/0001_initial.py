# Generated by Django 4.1.7 on 2023-03-24 08:02

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='Время создания')),
                ('started_date', models.DateTimeField(blank=True, verbose_name='Время начала')),
                ('timeToCookTotal', models.DurationField(blank=True, verbose_name='Время приготовления')),
                ('estimated_time', models.DurationField(default=datetime.timedelta(0), verbose_name='Осталось времени')),
                ('accepted', models.BooleanField(default=False, verbose_name='Принято в работу')),
                ('finished', models.BooleanField(default=False, verbose_name='Заказ выполнен')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Заказ',
                'verbose_name_plural': 'Заказы',
                'ordering': ('-created_date',),
            },
        ),
        migrations.CreateModel(
            name='OrderRecord',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveSmallIntegerField(default=1, verbose_name='Количество')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.order')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='products.product')),
            ],
            options={
                'verbose_name': 'Записи заказов',
                'verbose_name_plural': 'Записи заказов',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='products',
            field=models.ManyToManyField(through='orders.OrderRecord', to='products.product'),
        ),
        migrations.AddConstraint(
            model_name='orderrecord',
            constraint=models.UniqueConstraint(fields=('order', 'product'), name='order-elements'),
        ),
    ]
