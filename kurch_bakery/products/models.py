from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=200)
    timeToCook = models.DurationField(
                        verbose_name='Время приготовления HH:MM:SS')

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
    
    def __str__(self):
        return self.name
