from django.apps import AppConfig


class CoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'core'

    def ready(self):
        # Включаем планировщик для пересчета статусов
        from . import status_updater

        status_updater.start()
