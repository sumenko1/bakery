from apscheduler.schedulers.background import BackgroundScheduler

from orders.views import calc_estimated_time


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(calc_estimated_time, 'interval', seconds=5)
    scheduler.start()
